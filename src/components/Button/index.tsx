import React, { memo } from 'react';
import styled from 'styled-components';
import { TButtonProps } from './types';

const StyledButton = styled.button`
  min-width: 150px;
  padding: 10px 15px;
  border: 0;
  font-size: 16px;
  line-height: 16px;
  transition: 0.3s;
  color: white;
  background-color: skyblue;
  border-radius: 5px;
  cursor: pointer;

  :hover {
    background-color: deepskyblue;
  }

  &:active {
    opacity: 0.9;
  }

  :disabled {
    opacity: 0.6;
    background-color: skyblue;
  }
`;

export const Button = memo((
  {
    type = 'button',
    onClick,
    children,
    disabled = false,
  }: TButtonProps
) => {
  return (
    <StyledButton type={type} onClick={onClick} disabled={disabled}>
      {children}
    </StyledButton>
  )
});