export type WeatherCardProps = {
  date: string;
  minTemp: number;
  maxTemp: number;
  averageTemp: number;
  windSpeed: number;
  windDirectionInDeg: number;
  iconUrl: string;
  weatherStateName: string;
}