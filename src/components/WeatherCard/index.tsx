import moment from 'moment';
import React from 'react';

import { WeatherImage, WeatherInfoBlock, WindDirectionIcon, Wrapper } from './styled';

import { WeatherCardProps } from './types';

const propsFormat = 'YYYY-MM-DD';
const outputFormat = 'DD.MM.YYYY';
const kmInMile = 1.609;

/**
 * Карточка погоды
 */
export const WeatherCard = (
  {
    date,
    minTemp,
    maxTemp,
    averageTemp,
    windSpeed,
    windDirectionInDeg,
    iconUrl,
    weatherStateName
  }: WeatherCardProps
) => (
  <Wrapper>
    <WeatherImage src={iconUrl} alt={weatherStateName} />
    <WeatherInfoBlock>
      {moment(date, propsFormat).format(outputFormat)}
    </WeatherInfoBlock>
    <WeatherInfoBlock>
      <span>Min:&nbsp;</span>
      <span>{Math.round(minTemp)}&#8451;</span>
    </WeatherInfoBlock>
    <WeatherInfoBlock>
      <span>Max:&nbsp;</span>
      <span>{Math.round(maxTemp)}&#8451;</span>
    </WeatherInfoBlock>
    <WeatherInfoBlock>
      <span>Average:&nbsp;</span>
      <span>{Math.round(averageTemp)}&#8451;</span>
    </WeatherInfoBlock>
    <WeatherInfoBlock>
      <WindDirectionIcon
        src='https://www.metaweather.com/static/img/windarrow.svg'
        alt='arrow icon'
        windDirectionInDeg={Math.round(windDirectionInDeg)}
      />
      <span>&nbsp;{(windSpeed * kmInMile).toFixed(1)}km/h</span>
    </WeatherInfoBlock>
  </Wrapper>
);
