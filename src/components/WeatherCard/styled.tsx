import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: left;
  width: 100%;
  padding: 5px;
  margin-bottom: 10px;
`;

export const WeatherImage = styled.img`
  align-self: center;
  width: 60px;
  height: 60px;
  margin-bottom: 10px;
`;

export const WeatherInfoBlock = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 5px;
  font-size: 16px;
  line-height: 18px;

  :last-child {
    margin-bottom: 0;
  }
`;

export const WindDirectionIcon = styled.img<{windDirectionInDeg: number}>`
  position: relative;
  transform: rotate(${props => props.windDirectionInDeg ? props.windDirectionInDeg : 0}deg);
  width: 16px;
  height: 16px;
`;
