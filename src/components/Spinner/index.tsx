import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  0% {
    transform: rotate(0deg);
  }

  100% {
    transform: rotate(360deg);
  }
`;

export const Spinner = styled.div`
  width: 40px;
  height: 40px;
  border: 4px solid transparent;
  border-left: 4px solid black;
  border-radius: 50%;
  background: transparent;
  animation: ${rotate} 1.5s linear infinite;
  transform: translateZ(0);
`;