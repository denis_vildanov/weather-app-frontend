import { useCallback, useEffect, useState } from 'react';

import { GeoPositionStatus, TCurrentPosition, TCurrentPositionRetrying } from './types';

/**
 * Хук для использоватния позиции из геолокации.
 * А так же функции для попытки получить геолокацию снова.
 */
export const useGetCurrentPosition = () => {
  const [geoPosition, setGeoPosition] = useState<TCurrentPosition>(
  { status: GeoPositionStatus.initial, data: null }
  );

  const tryToGetGeoPosition = useCallback(
    () => {
      if (geoPosition.status === GeoPositionStatus.error) {
        const withUpdatesStatus: TCurrentPositionRetrying = {
          status: GeoPositionStatus.retrying,
          data: geoPosition.data
        };

        setGeoPosition(withUpdatesStatus)
      }

      const tenSeconds = 10 * 1000;
      const geo = navigator.geolocation;
      geo.getCurrentPosition(
        position => setGeoPosition({ status: GeoPositionStatus.success, data: position }),
        error => setGeoPosition({ status: GeoPositionStatus.error, data: error }),
        { timeout: tenSeconds }
      );
    },
    [geoPosition]
  );

  useEffect(
    () => {
      if (geoPosition.status === GeoPositionStatus.initial) {
        tryToGetGeoPosition();
      }
    },
    [tryToGetGeoPosition, geoPosition.status]
  );

  return { geoPosition, tryToGetGeoPosition };
}
