export enum GeoPositionStatus {
  initial = 'initial',
  error = 'error',
  success = 'success',
  retrying = 'retrying',
}

type TCurrentPositionInitial = {
  status: GeoPositionStatus.initial;
  data: null;
}

type TCurrentPositionError = {
  status: GeoPositionStatus.error;
  data: GeolocationPositionError;
}

export type TCurrentPositionRetrying = {
  status: GeoPositionStatus.retrying,
  data: GeolocationPositionError | null;
}

export type TCurrentPositionSuccess = {
  status: GeoPositionStatus.success;
  data: GeolocationPosition;
}

export type TCurrentPosition =
  TCurrentPositionInitial
  | TCurrentPositionError
  | TCurrentPositionRetrying
  | TCurrentPositionSuccess;