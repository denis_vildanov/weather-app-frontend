export enum DataStatus {
  Loading = 'Loading',
  Updating = 'Updating',
  Initial = 'initial',
  Success = 'Success',
  Error = 'Error',
}
