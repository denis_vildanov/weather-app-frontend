import React from 'react';

import { ErrorBoundary } from './containers/ErrorBoundary';
import { GeoPositionLayout } from './containers/GeoPositionLayout';
import { Weather } from './containers/Weather';

import './App.css';

function App() {

  return (
    <div className='App'>
      <ErrorBoundary>
        <GeoPositionLayout>
          {(geoPosition) => (<Weather geoPosition={geoPosition} />)}
        </GeoPositionLayout>
      </ErrorBoundary>
    </div>
  );
}

export default App;
