import axios from 'axios';
import React, { memo, useCallback, useState } from 'react';

import { Button } from '../../components/Button';
import { Spinner } from '../../components/Spinner';
import { WeatherCard } from '../../components/WeatherCard';

import { DataStatus } from '../../lib/enums/dataStatus';
import { OverlayWithCenteredChild, WeatherCardWrapper } from './styled';
import { StateError, StateInitial, StateLoading, StateSuccess, TWeather, TWeatherProps, TWeatherState } from './types';

const initialState: StateInitial = {
  status: DataStatus.Initial,
  data: null,
};

const getIconUrl = (shortName: string) => `https://www.metaweather.com/static/img/weather/${shortName}.svg`;

/**
 * Компонент погоды, рендерящий кнопку для запроса погоды, спиннер, ошибку или погоду в зависимости от состояния
 */
export const Weather = memo(({ geoPosition }: TWeatherProps) => {
  const [weather, setWeather] = useState<TWeatherState>(initialState);

  const getWeather = useCallback(
    async () => {
      if (geoPosition.status !== 'success') {
        return;
      }

      const loadingState: StateLoading = {
        status: DataStatus.Loading,
        data: null,
      }

      setWeather(loadingState);

      const { latitude, longitude } = geoPosition.data.coords;
      const body = {
        lat: latitude,
        lon: longitude,
      };

      try {
        const res = await axios.post<TWeather>('/weather', body);

        if (res.status === 200) {
          const newWeatherState: StateSuccess<TWeather> = {
            status: DataStatus.Success,
            data: res.data,
          };
          setWeather(newWeatherState);

          return;
        }
      } catch (e) {
        const newWeatherState: StateError = {
          status: DataStatus.Error,
          message: 'Cant get weather'
        };

        setWeather(newWeatherState);
        console.error(e);
      }
    },
    [geoPosition]
  );

  const onGetWeatherClick = useCallback(
    async (e: React.MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      await getWeather();
    },
    [getWeather]
  );

  if (weather.status === DataStatus.Initial) {
    return (
      <OverlayWithCenteredChild>
        <Button
          type='button'
          onClick={onGetWeatherClick}
        >
          Get weather
        </Button>
      </OverlayWithCenteredChild>
    );
  }

  if (weather.status === DataStatus.Loading || weather.status === DataStatus.Updating) {
    return (
      <OverlayWithCenteredChild>
        <Spinner />
      </OverlayWithCenteredChild>
    );
  }

  if (weather.status === DataStatus.Error) {
    return <p>{weather.message}</p>;
  }

  return (
    <div>
      <h1>{weather.data.title}</h1>
      <WeatherCardWrapper>
        {
          weather.data.consolidatedWeather.map(
            (item) =>
              <WeatherCard
                key={item.id}
                averageTemp={item.theTemp}
                date={item.applicableDate}
                iconUrl={getIconUrl(item.weatherStateAbbr)}
                maxTemp={item.maxTemp}
                minTemp={item.minTemp}
                weatherStateName={item.weatherStateName}
                windDirectionInDeg={item.windDirection}
                windSpeed={item.windSpeed}
              />
          )
        }
      </WeatherCardWrapper>
    </div>
  );
});