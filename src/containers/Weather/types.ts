import { Union } from 'ts-toolbelt';
import { TCurrentPositionSuccess } from '../../hooks/useGetCurrentPosition/types';
import { DataStatus } from '../../lib/enums/dataStatus';

export type TWeatherProps = {
  geoPosition: TCurrentPositionSuccess;
}

export type IConsolidatedWeather = {
  id: number;
  applicableDate: string;
  weatherStateName: string;
  weatherStateAbbr: string;
  windSpeed: number;
  windDirection: number;
  windDirectionCompass: string;
  minTemp: number;
  maxTemp: number;
  theTemp: number;
  airPressure: number;
  humidity: number;
  visibility: number;
  predictability: number;
}

type ISources = {
  url: string;
  title: string;
}

export type TWeather = {
  title: string;
  locationType: string;
  lattLong: string;
  time: string;
  sunRise: string;
  sunSet: string;
  timezoneName: string;
  parent: {
    title: string;
    location_type: string;
    latt_long: string;
    woeid: number;
  };
  consolidatedWeather: IConsolidatedWeather[];
  sources: ISources[];
}

export type StateSuccess<T> = {
  status: DataStatus.Success;
  data: T;
}

export type StateError = {
  status: DataStatus.Error;
  message: string;
}

export type StateLoading = {
  status: DataStatus.Loading;
  data: null;
}

export type StateUpdating = {
  status: DataStatus.Updating;
  data: null;
}

export type StateInitial = {
  status: DataStatus.Initial;
  data: null;
}

export type EntityState<T> = Union.Strict<StateInitial | StateUpdating | StateLoading | StateSuccess<T> | StateError>;

export type TWeatherState = EntityState<TWeather>;
