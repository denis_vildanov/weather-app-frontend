import React from 'react';

import { TErrorBoundaryProps, TErrorBoundaryState } from './types';

/**
 * Контейнер для отлова ошибок в приложении
 */
export class ErrorBoundary extends React.Component<TErrorBoundaryProps, TErrorBoundaryState> {
  constructor(props: TErrorBoundaryProps) {
    super(props);

    this.state = { hasError: false };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    console.error('Error: ', error);
    console.error('ErrorInfo: ', errorInfo);
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return <p>Something went wrong</p>
    }

    return this.props.children;
  }
}