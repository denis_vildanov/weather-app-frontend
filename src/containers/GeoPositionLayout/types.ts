import React from 'react';

import { TCurrentPositionSuccess } from '../../hooks/useGetCurrentPosition/types';

export type TGeoPositionLayoutProps = {
  children: (value: TCurrentPositionSuccess) => React.ReactNode
}
