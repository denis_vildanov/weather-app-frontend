import React from 'react';

import { Button } from '../../components/Button';
import { Spinner } from '../../components/Spinner';
import { SpinnerWrapper } from './styled';

import { useGetCurrentPosition } from '../../hooks/useGetCurrentPosition/useGetCurrentPosition';

import { GeoPositionStatus } from '../../hooks/useGetCurrentPosition/types';
import { TGeoPositionLayoutProps } from './types';

/**
 * Лейаут для использования геопозиции
 */
export const GeoPositionLayout = (props: TGeoPositionLayoutProps) => {
  const { geoPosition, tryToGetGeoPosition } = useGetCurrentPosition();

  const onClickTryAgain = (e: any) => {
    e.preventDefault();

    tryToGetGeoPosition();
  }

  if (geoPosition.status === GeoPositionStatus.initial) {
    return (
      <SpinnerWrapper>
        <Spinner/>
      </SpinnerWrapper>
    );
  }

  if (geoPosition.status === GeoPositionStatus.error) {
    return (
      <>
        <p>{geoPosition.data.message}</p>
        <Button onClick={onClickTryAgain}>
          Try again
        </Button>
      </>
    );
  }

  if (geoPosition.status === GeoPositionStatus.retrying) {
    return <p>Trying to get your position again</p>
  }

  return (
    <>
      {props.children(geoPosition)}
    </>
  );
};
